##
## Python module for storing measures to MongoDB.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from core import measures_sink
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, PyMongoError
import logging


class MongoDBSink(measures_sink.MeasuresSink):
    """Measures sink that interfaces with MongoDB
       (https://www.mongodb.com/).

    Inserts collected data into the given database and collection.
    Note, it is not necessary to create the database and the collection in
    advance as MongoDB implicitly will create them the first time they are
    referenced in a command.
    In some situations, you may want to explicitly create them (e.g., to set
    some options, like the maximum size or the documentation validation rules).
    To do so, you can create them from the MongoDB shell as follows (where
    database is 'easycloud' and collection is 'measures'):

        ```
        use easycloud

        db.createCollection('measures')
        ```

    Tested with MongoDB v. 4.4 and MongoDB's Python Driver v. 3.11.0.

    References:
    - MongoDB: https://www.mongodb.com/
    - DataStax's Python Driver: https://docs.mongodb.com/drivers/pymongo
    """

    DEFAULT_HOSTS = ['localhost'] # A list of hostnames (possibly including the port number) or URIs (e.g., 'mongodb://user:password@host:port') running MongoDB
    DEFAULT_PORT = 27017 # The server port (will be overridden by any port in the host string
    DEFAULT_DB = 'easycloud'
    DEFAULT_COLLECTION = 'measures'

    @classmethod
    def from_conf(cls, conf):
        hosts = None
        if 'hosts' in conf:
            hosts = [ h.strip() for h in conf['hosts'].split(',') ]
        port = None
        if 'port' in conf and conf['port'] is not None:
            port = int(conf['port'])
        db = None
        if 'db' in conf:
            db = conf['db']
        collection = None
        if 'collection' in conf:
            collection = conf['collection']
        return cls(hosts=hosts, port=port, db=db, collection=collection)

    def __init__(self, hosts=DEFAULT_HOSTS, port=DEFAULT_PORT, db=DEFAULT_DB, collection=DEFAULT_COLLECTION):
        super(MongoDBSink, self).__init__()
        self._hosts = hosts if hosts is not None and len(hosts) > 0 else self.DEFAULT_HOSTS
        self._port = port if port is not None and port >= 0 else self.DEFAULT_PORT
        self._db = db if db is not None and len(db) > 0 else self.DEFAULT_DB
        self._collection = collection if collection is not None and len(collection) > 0 else self.DEFAULT_COLLECTION


    def put(self, measure):
        client = None
        try:
            client = MongoClient(self._hosts,
                                 port=self._port)
            #try:
            #    # The ismaster command is cheap and does not require auth.
            #    client.admin.command('ismaster')
            #except ConnectionFailure:
            #    logging.warning("Server not available")

            db = client[self._db]
            db[self._collection].insert_one(self._make_document(measure))
        except PyMongoError as e:
            logging.warning('Unable to insert measure: {}'.format(e))
        if client is not None:
            client.close()

    def mput(self, measures):
        if len(measures) > 0:
            docs = []
            for measure in measures:
                docs.append(self._make_document(measure))
            client = None
            try:
                client = MongoClient(self._hosts,
                                     port=self._port)
                #try:
                #    # The ismaster command is cheap and does not require auth.
                #    client.admin.command('ismaster')
                #except ConnectionFailure:
                #    logging.warning("Server not available")

                db = client[self._db]
                db[self._collection].insert_many(docs)
            except PyMongoError as e:
                logging.warning('Unable to insert measure: {}'.format(e))
            if client is not None:
                client.close()

    def _make_document(self, measure):
        return {'timestamp': measure.timestamp.isoformat(),
                'object-id': measure.object_id,
                'object_ns': measure.object_ns,
                'metric': measure.metric,
                'unit': measure.unit,
                'value': measure.value}
