##
## Python module for storing measures to plain files.
##
## Copyright 2020 Marco Guazzone (marco.guazzone@gmail.com)
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

from core import measures_sink
import csv
import logging
import os
import tempfile


class FileSink(measures_sink.MeasuresSink):
    """Measures sink that exports measures to plain files."""

    DEFAULT_BASEPATH = os.path.join(tempfile.gettempdir(), 'easycloud')

    @classmethod
    def from_conf(cls, conf):
        basepath = None
        if 'basepath' in conf:
            basepath = conf['basepath']
        return cls(basepath=basepath)

    def __init__(self, basepath=DEFAULT_BASEPATH):
        super(FileSink, self).__init__()
        self._basepath = basepath if basepath is not None and len(basepath) > 0 else self.DEFAULT_BASEPATH
        #self._basepath_tmp = os.path.join(self.basepath_, 'tmp')
        os.makedirs(self._basepath, exist_ok=True)

    #@staticmethod
    #def _list_to_nested_dict(lst, val0 = None):
    #    if len(lst) > 0:
    #        key = lst.pop(0)
    #        return dict({key: _list_to_nested_dict(lst, val0)})
    #    else
    #        return val0


class CsvFileSink(FileSink):
    """File-based measures sink that exports measure to CSV files.

    This sink will create a separate CSV file for each measure group and will
    save it inside a given base path.
    """

    def __init__(self, basepath = FileSink.DEFAULT_BASEPATH):
        """Create a new sink which saves CSV files inside the given basepath.

        Args:
            basepath (str): the path where to store CSV files.
        """
        #TODO: add arguments for configuration CSV writer (e.g., field separator, ...)
        super(CsvFileSink, self).__init__(basepath)

    def put(self, measure):
        #os.makedirs(self._measure_path(measure), exist_ok=True)
        group_id = self._measure_group_encode(self._measure_group(measure))
        csvfname = os.path.join(self._basepath, group_id + ".csv")
        write_header = False
        if not os.path.isfile(csvfname):
            write_header = True
        with open(csvfname, 'a') as csvfile:
            csvwr = csv.writer(csvfile)
            if write_header:
                self._write_csv_header(csvwr)
            self._write_csv_measure(csvwr, measure)
        logging.debug("Measure stored to file {}".format(csvfname))

    def mput(self, measures):
        # Groups measures by object namespace, object ID and metric to optimize writes on CSV files
        measures_groups = dict() # {namespace => {object-id => {metric => [measure1, measure2, ...]}}}
        for measure in measures:
            group_id = self._measure_group_encode(self._measure_group(measure))
            if group_id not in measures_groups:
                measures_groups[group_id] = []
            measures_groups[group_id].append(measure)
        for group_id in measures_groups:
            csvfname = os.path.join(self._basepath, group_id + ".csv")
            write_header = False
            if not os.path.isfile(csvfname):
                write_header = True
            with open(csvfname, 'a') as csvfile:
                csvwr = csv.writer(csvfile)
                if write_header:
                    self._write_csv_header(csvwr)
                for measure in measures_groups[group_id]:
                    self._write_csv_measure(csvwr, measure)
            logging.debug("Measure stored to file {}".format(csvfname))

    #def _measure_path(self, measure):
    #    path = self._basepath
    #    for group in self._measure_group(measure)[-1]:
    #        path = os.path.join(path, group)
    #    return path

    #def _measure_filename(self, measure):
    #    fname = self._valid_filename(measure.metric)
    #    #return os.path.join(self._basepath, base64.urlsafe_b64encode(instance_id), metric_name)
    #    return os.path.join(self._measure_path(measure), fname) + ".csv"

    ##def _build_measure_path(self, instance_id, random_id=None):
    ##    path = self._measure_path(instance_id)
    ##    if random_id:
    ##        if random_id is True:
    ##            now = datetime.datetime.utcnow().strftime("_%Y%m%d_%H:%M:%S")
    ##            random_id = uuid.uuid4() + now
    ##        return os.path.join(path, random_id)
    ##    return path

    #@staticmethod
    #def _valid_filename(s):
    #    """
    #    Returns the given string converted to a string that can be used for a
    #    safe filename.
    #    """
    #    ##This function performs the following changes:
    #    ##- it removes leading and trailing spaces,
    #    ##- it converts other spaces to underscores, and
    #    ##- it removes anything that is not an alphanumeric, dash, underscore, or
    #    ##  dot.
    #    #s = str(s).strip().replace(' ', '_')
    #    #return re.sub(r'(?u)[^-\w.]', '', s)
    #    
    #    #This function performs the following changes:
    #    #- it removes leading and trailing spaces,
    #    #- it converts other spaces and special characters to underscores.
    #    return str(s).strip().replace(os.sep, '_').replace(os.altsep, '_')

    @staticmethod
    def _write_csv_header(csvwr):
        csvwr.writerow(["Timestamp", "Object Namespace", "Object ID", "Metric", "Unit", "Value"])

    @staticmethod
    def _write_csv_measure(csvwr, measure):
        csvwr.writerow([measure.timestamp.isoformat(), measure.object_ns, measure.object_id, measure.metric, measure.unit, measure.value])

