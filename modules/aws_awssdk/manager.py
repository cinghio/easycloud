"""
EasyCloud Amazon Web Services Manager
"""

# For a list of valid filters to use with the ex_filter parameter of certain methods,
# please visit https://docs.aws.amazon.com/en_us/AWSEC2/latest/APIReference/API_Operations.html

import time

import boto3
from core.compute import Instance, InstanceStatus
from core.actionbinder import bind_action
from core.metamanager import MetaManager
from modules.aws_awssdk.actions import AWSAgentActions
from modules.aws_awssdk.confmanager import AWSConfManager
from modules.aws_awssdk.monitor import AWSMonitor
from tui.simpletui import SimpleTUI


class AWSInstance(Instance):
    #See: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#instance

    NODE_STATE_MAP_ = {
        'pending': InstanceStatus.PENDING,
        'stopped': InstanceStatus.STOPPED,
        'stopping': InstanceStatus.STOPPING,
        'running': InstanceStatus.RUNNING,
        'shutting-down': InstanceStatus.UNKNOWN,
        'terminated': InstanceStatus.TERMINATED
    }


    def __init__(self, ec2_conn, ec2_instance):
        # Example: Instances: {'Reservations': [{'Groups': [], 'Instances': [{'AmiLaunchIndex': 0, 'ImageId': 'ami-1234abcd', 'InstanceId': 'i-f899088eef3553264', 'InstanceType': 'm1.small', 'KernelId': 'None', 'KeyName': 'None', 'LaunchTime': datetime.datetime(2020, 10, 23, 8, 46, 17, tzinfo=tzutc()), 'Monitoring': {'State': 'disabled'}, 'Placement': {'AvailabilityZone': 'us-west-1a', 'GroupName': '', 'Tenancy': 'default'}, 'PrivateDnsName': 'ip-10-86-212-159.us-west-1.compute.internal', 'PrivateIpAddress': '10.86.212.159', 'ProductCodes': [], 'PublicDnsName': 'ec2-54-214-73-237.us-west-1.compute.amazonaws.com', 'PublicIpAddress': '54.214.73.237', 'State': {'Code': 16, 'Name': 'running'}, 'StateTransitionReason': '', 'SubnetId': 'subnet-ae8407fa', 'VpcId': 'vpc-cabd22c4', 'Architecture': 'x86_64', 'BlockDeviceMappings': [{'DeviceName': '/dev/sda1', 'Ebs': {'AttachTime': datetime.datetime(2020, 10, 23, 8, 46, 17, tzinfo=tzutc()), 'DeleteOnTermination': True, 'Status': 'in-use', 'VolumeId': 'vol-e2cbe3db'}}], 'ClientToken': 'ABCDE1234567890123', 'EbsOptimized': False, 'Hypervisor': 'xen', 'NetworkInterfaces': [{'Association': {'IpOwnerId': '123456789012', 'PublicIp': '54.214.73.237'}, 'Attachment': {'AttachTime': datetime.datetime(2015, 1, 1, 0, 0, tzinfo=tzutc()), 'AttachmentId': 'eni-attach-22b6793d', 'DeleteOnTermination': True, 'DeviceIndex': 0, 'Status': 'attached'}, 'Description': 'Primary network interface', 'Groups': [], 'MacAddress': '1b:2b:3c:4d:5e:6f', 'NetworkInterfaceId': 'eni-361059cc', 'OwnerId': '123456789012', 'PrivateIpAddress': '10.86.212.159', 'PrivateIpAddresses': [{'Association': {'IpOwnerId': '123456789012', 'PublicIp': '54.214.73.237'}, 'Primary': True, 'PrivateIpAddress': '10.86.212.159'}], 'SourceDestCheck': True, 'Status': 'in-use', 'SubnetId': 'subnet-ae8407fa', 'VpcId': 'vpc-cabd22c4'}], 'RootDeviceName': '/dev/sda1', 'RootDeviceType': 'ebs', 'SecurityGroups': [], 'SourceDestCheck': True, 'StateReason': {'Code': '', 'Message': ''}, 'VirtualizationType': 'paravirtual'}, {'AmiLaunchIndex': 1, 'ImageId': 'ami-1234abcd', 'InstanceId': 'i-5b6b6e8152f8ffe91', 'InstanceType': 'm1.small', 'KernelId': 'None', 'KeyName': 'None', 'LaunchTime': datetime.datetime(2020, 10, 23, 8, 46, 17, tzinfo=tzutc()), 'Monitoring': {'State': 'disabled'}, 'Placement': {'AvailabilityZone': 'us-west-1a', 'GroupName': '', 'Tenancy': 'default'}, 'PrivateDnsName': 'ip-10-104-239-182.us-west-1.compute.internal', 'PrivateIpAddress': '10.104.239.182', 'ProductCodes': [], 'PublicDnsName': 'ec2-54-214-44-189.us-west-1.compute.amazonaws.com', 'PublicIpAddress': '54.214.44.189', 'State': {'Code': 16, 'Name': 'running'}, 'StateTransitionReason': '', 'SubnetId': 'subnet-ae8407fa', 'VpcId': 'vpc-cabd22c4', 'Architecture': 'x86_64', 'BlockDeviceMappings': [{'DeviceName': '/dev/sda1', 'Ebs': {'AttachTime': datetime.datetime(2020, 10, 23, 8, 46, 17, tzinfo=tzutc()), 'DeleteOnTermination': True, 'Status': 'in-use', 'VolumeId': 'vol-df8b8f8f'}}], 'ClientToken': 'ABCDE1234567890123', 'EbsOptimized': False, 'Hypervisor': 'xen', 'NetworkInterfaces': [{'Association': {'IpOwnerId': '123456789012', 'PublicIp': '54.214.44.189'}, 'Attachment': {'AttachTime': datetime.datetime(2015, 1, 1, 0, 0, tzinfo=tzutc()), 'AttachmentId': 'eni-attach-22e8a157', 'DeleteOnTermination': True, 'DeviceIndex': 0, 'Status': 'attached'}, 'Description': 'Primary network interface', 'Groups': [], 'MacAddress': '1b:2b:3c:4d:5e:6f', 'NetworkInterfaceId': 'eni-ba9fb0b9', 'OwnerId': '123456789012', 'PrivateIpAddress': '10.104.239.182', 'PrivateIpAddresses': [{'Association': {'IpOwnerId': '123456789012', 'PublicIp': '54.214.44.189'}, 'Primary': True, 'PrivateIpAddress': '10.104.239.182'}], 'SourceDestCheck': True, 'Status': 'in-use', 'SubnetId': 'subnet-ae8407fa', 'VpcId': 'vpc-cabd22c4'}], 'RootDeviceName': '/dev/sda1', 'RootDeviceType': 'ebs', 'SecurityGroups': [], 'SourceDestCheck': True, 'StateReason': {'Code': '', 'Message': ''}, 'VirtualizationType': 'paravirtual'}], 'OwnerId': '123456789012', 'ReservationId': 'r-b6b42c80'}], 'ResponseMetadata': {'RequestId': 'fdcdcab1-ae5c-489e-9c33-4637c5dda355', 'HTTPStatusCode': 200, 'HTTPHeaders': {'server': 'amazon.com'}, 'RetryAttempts': 0}}

        self.ec2_conn_ = ec2_conn
        self.ec2_inst_ = ec2_instance
        #See: https://github.com/apache/libcloud/blob/trunk/libcloud/compute/drivers/ec2.py
        #See: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_instances
        self.state_ = self.NODE_STATE_MAP_.get(self.ec2_inst_['State']['Name'], InstanceStatus.UNKNOWN)
        ##TODO: fill these fields: public_ips, private_ips, driver, size, image, extra, created_at
        self.extra_ = {'availability': self.ec2_inst_['Placement']['AvailabilityZone'] if 'Placement' in self.ec2_inst_ else None,
                       'groups': [ {'group_name': group['GroupName'], 'group_id': group['GroupId']} for group in self.ec2_inst_['SecurityGroups'] ],
                       'instance_type': self.ec2_inst_['InstanceType'],
                       'key_name': self.ec2_inst_.get('KeyName', None)}
        if 'Tags' in self.ec2_inst_:
            tags = { tag['Key']:tag['Value'] for tag in self.ec2_inst_['Tags'] }
            self.name_ = tags.get('Name', self.ec2_inst_['InstanceId'])
        else:
            self.name_ = self.ec2_inst_['InstanceId']
        #FIXME: libcloud also creates a UUID (see: https://github.com/apache/libcloud/blob/fe4f81b5a31a8f470ff6ba5af7451cccfc423c2b/libcloud/compute/base.py)

    @property
    def id(self):
        return self.ec2_inst_['InstanceId']

    @property
    def name(self):
        #return self.tags_['Name'] if 'Name' in self.tags_ else self.ec2_inst_['InstanceId']
        return self.name_

    @property
    def state(self):
        return self.state_

    @property
    def extra(self):
        return self.extra_

    def start(self):
        self.ec2_conn_.start_instances(InstanceIds=[self.ec2_inst_['InstanceId']]) #, DryRun=True)

    def stop_node(self):
        self.ec2_conn_.stop_instances(InstanceIds=[self.ec2_inst_['InstanceId']]) #, DryRun=True)

    def __repr__(self):
        state = InstanceStatus.tostring(self.state_)

        #FIXME: print UUID?
        return ('<Instance: id={}, name={}, state={}, public_ips={}, private_ips={}, provider={} ...>'.format(self.ec2_inst_['InstanceId'], self.name_, state, None, None, None))


class AWS(MetaManager):

    def __init__(self, config_file=None, rule_file=None):
        super().__init__(rule_file=rule_file)
        self.platform_name = "Amazon Web Services"
        self.conf = AWSConfManager(config_file)
        self.cloned_instances = []
        # self.snapshots = None
        self.connect()

    # =============================================================================================== #
    #                                Platform-specific client creation                                #
    # =============================================================================================== #

    def connect(self):
        """
        Connection to Amazon Web Services
        """
        # See also:
        # - https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.start_instances
        # - https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-examples.html
        # - https://blog.codecentric.de/en/2020/01/testing-aws-python-code-with-moto/
        # - https://github.com/spulec/moto

        # Alternative #1: Low-level (thread-safe) interface
        self.ec2_client = boto3.client('ec2',
                                       aws_access_key_id=self.conf.ec2_access_key_id,
                                       aws_secret_access_key=self.conf.ec2_secret_access_key,
                                       aws_session_token=self.conf.ec2_session_token,
                                       region_name=self.conf.ec2_default_region)
        ## Alternative #2: High-level (not thread-safe) interface
        #session_ = boto3.session.Session(aws_access_key_id=self.conf.ec2_access_key_id,
        #                                 aws_secret_access_key=self.conf.ec2_secret_access_key,
        #                                 aws_session_token=self.conf.ec2_session_token,
        #                                 region_name=self.conf.ec2_default_region)
        #self.ec2_conn_ = session.resources('ec2')

    # =============================================================================================== #
    #                                  Platform-specific list printers                                #
    # =============================================================================================== #

    # Nothing here

    # =============================================================================================== #
    #                                         List builders                                           #
    # =============================================================================================== #

    def _platform_list_all_images(self):
        """
        Print all available images
        Format: "ID", "Name", Image ID", "State"
        """
        if self.conf.freetier_only:
            # Use free-tier images
            self.images = self.ec2_client.list_images(ex_image_ids=self.conf.freetier_images_ids)
        else:
            # Use image filters
            self.images = self.ec2_client.list_images(
                ex_filters={"name": self.conf.images_filters})
        i = 1
        table_body = []
        for image in self.images:
            table_body.append([i, image.name, image.id, image.extra["state"]])
            i = i + 1
        return table_body

    def _platform_list_all_availability_zones(self):
        """
        Print all available images
        Format: "ID", "Name", Zone State", "Region Name"
        """
        self.avail_zones = self.ec2_client.list_locations()
        i = 1
        table_body = []
        for avail_zone in self.avail_zones:
            table_body.append([i, avail_zone.availability_zone.name,
                               avail_zone.availability_zone.zone_state, avail_zone.availability_zone.region_name])
            i = i + 1
        return table_body

    def _platform_list_all_instance_types(self):
        """
        Print all instance types
        Format: "ID", "Instance Type ID", "vCPUs", "Ram (GB)", "Disk (GB)"
        """
        self.instance_types = self.ec2_client.list_sizes()
        if self.conf.freetier_only:
            filtered_instance_types = []
            for instance_type in self.instance_types:
                if instance_type.id in self.conf.freetier_instance_types:
                    filtered_instance_types.append(instance_type)
                self.instance_types = filtered_instance_types
        i = 1
        table_body = []
        for instance_type in self.instance_types:
            table_body.append([i, instance_type.id, instance_type.extra["vcpu"],
                               instance_type.ram / 1024,
                               str(instance_type.disk) + " (" + instance_type.extra["storage"] + ")"])
            i = i + 1
        return table_body

    def _platform_list_all_security_groups(self):
        """
        Print all security groups
        Format: "ID", "SG name", "SG description"
        """
        # Here a list of str is returned (the docs say list of EC2SecurityGroup)
        self.security_groups = self.ec2_client.ex_list_security_groups()
        i = 1
        table_body = []
        for security_group in self.security_groups:
            table_body.append([i, security_group, "n/a"])
            i = i + 1
        return table_body

    def _platform_list_all_networks(self):
        """
        Print id and other useful informations of all networks available
        Format: "ID", "Network name", "Description"
        """
        self.networks = self.ec2_client.ex_list_networks()
        i = 1
        table_body = []
        for network in self.networks:
            table_body.append([i, network.name, network.id])
            i = i + 1
        return table_body

    def _platform_list_all_key_pairs(self):
        """
        Print all key pairs
        Format: "ID", "Key name", "Key fingerprint"
        """
        self.key_pairs = self.ec2_client.list_key_pairs()
        i = 1
        table_body = []
        for key_pair in self.key_pairs:
            table_body.append([i, key_pair.name, key_pair.fingerprint])
            i = i + 1
        return table_body

    def list_instances(self):
        """
        Returns a list of instance objects each of which has the following structure:
            id (str) – Node ID.
            name (str) – Node name.
            state (libcloud.compute.types.NodeState) – Node state.
            public_ips (list) – Public IP addresses associated with this node.
            private_ips (list) – Private IP addresses associated with this node.
            driver (NodeDriver) – Driver this node belongs to.
            size (NodeSize) – Size of this node. (optional)
            image (NodeImage) – Image of this node. (optional)
            created_at – The datetime this node was created (optional)
            extra (dict) – Optional provider specific attributes associated with this node.
        """
        instances = []
        for reservation in self.ec2_client.describe_instances()['Reservations']:
            for raw_instance in reservation['Instances']:
                instance = AWSInstance(self.ec2_client, raw_instance)
                instances.append(instance)
        return instances

    def _platform_list_all_instances(self):
        """
        Print instance id, image id, IP address and state for each active instance
        Format: "ID", "Instance Name", "Instance ID", "IP address", "Status", "Key Name", "Avail. Zone"
        """
        self.instances = self.list_instances()
        i = 1
        table_body = []
        for instance in self.instances:
            if len(instance.public_ips) > 0 and None not in instance.public_ips:
                table_body.append([i, instance.name, instance.id, ", ".join(instance.public_ips), instance.state, instance.extra["key_name"], instance.extra["availability"]])
            else:
                table_body.append([i, instance.name, instance.id, "-", instance.state, instance.extra["key_name"], instance.extra["availability"]])
            i = i + 1
        return table_body

    def _platform_list_all_volumes(self):
        """
        Print volumes alongside informations regarding attachments
        Format: "Volume ID", "Creation", "Size (GB)", "Attached To", "Status"
        """
        self.volumes = self.ec2_client.list_volumes()
        i = 1
        table_body = []
        for volume in self.volumes:
            created_at = volume.extra["create_time"].strftime("%b %d %Y, %H:%M:%S") + " UTC"
            if volume.extra["instance_id"] is not None:
                node = self.ec2_client.list_nodes(ex_node_ids=[volume.extra["instance_id"]])[0]
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   node.name + " (" + volume.extra["device"] + ")", volume.state, volume.extra["zone"]])
            else:
                table_body.append([i, volume.name, volume.id, created_at, volume.size,
                                   "- (-)", volume.state, volume.extra["zone"]])
            i = i + 1
        return table_body

    def _platform_list_all_floating_ips(self):
        """
        Print ip and other useful information of all floating ip available
        Format: ""ID", "Public Ip", "Floating IP ID", "Associated Instance"
        """
        self.floating_ips = self.ec2_client.ex_describe_all_addresses()
        i = 1
        table_body = []
        for floating_ip in self.floating_ips:
            if(floating_ip.instance_id is not None):
                node = self.ec2_client.list_nodes(ex_node_ids=[floating_ip.instance_id])[0]
                if(node is not None):
                    table_body.append([i, floating_ip.ip, floating_ip.extra["allocation_id"], node.name, "n/a"])
                else:
                    table_body.append([i, floating_ip.ip, floating_ip.extra["allocation_id"], "Load Balancer", "n/a"])
            else:
                table_body.append([i, floating_ip.ip, floating_ip.extra["allocation_id"], "-", "n/a"])
            i = i + 1
        return table_body

    # =============================================================================================== #
    #                                Platform-specific list builders                                  #
    # =============================================================================================== #

    # Nothing here

    # =============================================================================================== #
    #                                       Actions and Menus                                         #
    # =============================================================================================== #

    def _platform_get_region(self):
        """
        Get current region name

        Returns:
            str: the name of the current region
        """
        return self.conf.ec2_default_region

    def _platform_create_new_instance(self, instance_name, image, instance_type, monitor_cmd_queue=None):
        """
        Create a new instance using the Amazon Web Services API
        Some specific steps are performed here:
            - Ask for security group
            - Ask for key pair
            - Instance creation summary

        Args:
            instance_name (str): The name of the instance
            image (<image_type>): The image to be used as a base system
            instance_type (<instance_type>): The VM flavor
            monitor_cmd_queue: the monitor commands Quue
        """
        # 5. Security Group
        security_group_index = SimpleTUI.list_dialog("Security Groups available",
                                                     self.print_all_security_groups,
                                                     question="Select security group")
        if security_group_index is None:
            return
        security_group = self.security_groups[security_group_index - 1]
        # 6. Key Pair
        key_pair_index = SimpleTUI.list_dialog("Key Pairs available",
                                               self.print_all_key_pairs,
                                               question="Select key pair")
        if key_pair_index is None:
            return
        key_pair = self.key_pairs[key_pair_index - 1]

        # Creation summary
        print("\n--- Creating a new instance with the following properties:")
        print("- %-20s %-30s" % ("Name", instance_name))
        print("- %-20s %-30s" % ("Image", image.name))
        print("- %-20s %-30s" % ("Instance Type", instance_type.name))
        print("- %-20s %-30s" % ("Key Pair", key_pair.name))
        print("- %-20s %-30s" % ("Security Group", security_group))

        # ask for confirm
        print("")
        if(SimpleTUI.user_yn("Are you sure?")):
            # ex_monitoring is an invalid argument for create_node()
            '''
            instance = self.ec2_client.create_node(name=instance_name,
                                                   image=image,
                                                   size=instance_type,
                                                   ex_keyname=key_pair.name,
                                                   ex_security_groups=[security_group],
                                                   ex_monitoring=True,
                                                   ex_mincount=1,
                                                   ex_maxcount=1)
                                                   '''
            instance = self.ec2_client.create_node(name=instance_name,
                                                   image=image,
                                                   size=instance_type,
                                                   ex_keyname=key_pair.name,
                                                   ex_security_groups=[security_group],
                                                   ex_mincount=1,
                                                   ex_maxcount=1)
            if instance is None:
                return False
            if monitor_cmd_queue is not None and self.is_monitor_running():
                monitor_cmd_queue.put({"command": "add", "instance_id": instance.id})
            return True

    def _platform_instance_action(self, instance, action):
        """
        Handle an instance action with Amazon Web Services API
        """
        if action == "reboot":
            return instance.reboot()
        elif action == "delete":
            return instance.destroy()

    def _platform_get_instance_info(self):
        """
        Return a list of instances info
        """
        info = []
        for instance in self.ec2_client.list_nodes():
            info.append({"id": instance.id, "name": instance.name})
        return info

    def _platform_is_volume_attached(self, volume):
        """
        Check if a volume is attached to an instance

        Args:
            volume (<volume>): The volume to check

        Returns:
            bool: True if the volume is successfully attached, False otherwise
        """
        if volume.extra["instance_id"] is None:
            return False
        return True

    def _platform_detach_volume(self, volume):
        """
        Detach a volume using the Amazon Web Services API

        Args:
            volume (<volume>): The volume to detach

        Returns:
            bool: True if the volume is successfully detached, False otherwise
        """
        result = self.ec2_client.detach_volume(volume)
        if result:
            while True:
                # No direct way to refresh a Volume status, so we look if
                # it is still attached to its previous instance
                node = self.ec2_client.list_nodes(ex_node_ids=[volume.extra["instance_id"]])[0]
                node_volumes = self.ec2_client.list_volumes(node=node)
                is_present = False
                for node_volume in node_volumes:
                    if node_volume.id == volume.id:
                        is_present = True
                        break
                if not is_present:
                    break
                time.sleep(3)
        return result

    def _platform_delete_volume(self, volume):
        """
        Delete a volume using the Amazon Web Services API

        Args:
            volume (<volume>): The volume to delete

        Returns:
            bool: True if the volume is successfully deleted, False otherwise
        """
        return self.ec2_client.destroy_volume(volume)

    def _platform_attach_volume(self, volume, instance):
        """
        Attach a volume using the Amazon Web Services API
        Some specific steps are performed here:
            - Exposure point (e.g. /dev/sdb)

        Args:
            volume (<volume>): The volume to attach
            instance (<instance>): The instance where the volume is to be attached

        Returns:
            bool: True if the volume is attached successfully, False otherwise
        """
        # Ask for exposure point (the GNU/Linux device where this volume will
        # be available)
        exposure_point = SimpleTUI.input_dialog("Exposure point",
                                                question="Specify where the device is exposed, e.g. ‘/dev/sdb’",
                                                return_type=str,
                                                regex="^(/[^/ ]*)+/?$")
        if exposure_point is None:
            return
        return self.ec2_client.attach_volume(instance, volume, exposure_point)

    def _platform_create_volume(self, volume_name, volume_size):
        """
        Create a new volume using the Amazon Web Services API
        Some specific steps are performed here:
            - Volume type (standard or io1)
            - IOPS (only if io1 is selected)
            - Zone selection (required)

        Args:
            volume_name (str): Volume name
            volume_size (int): Volume size in GB

        Returns:
            bool: True if the volume is successfully created, False otherwise
        """

        # Volume type
        volume_type = SimpleTUI.input_dialog("Volume type",
                                             question="Specify the volume type (standard, io1, gp2, st1, sc1)",
                                             return_type=str,
                                             regex="^(standard|io1|gp2|st1|sc1)$")
        if volume_type is None:
            return

        min_size = 0
        max_size = 16384

        if volume_type == "gp2":
            min_size = 1

        if volume_type == "io1":
            min_size = 4

        if volume_type == "sc1":
            min_size = 500

        if volume_type == "st1":
            min_size = 500

        if volume_type == "standard":
            min_size = 1
            max_size = 1024

        if volume_size < min_size or volume_size > max_size:
            volume_size = SimpleTUI.input_dialog("Size",
                                          question="Specify the size [Min: " + str(min_size) + " | Max: " + str(max_size) + " GiB]",
                                          return_type=int)
        
        if volume_size < min_size or volume_size > max_size:
            return

        # IOPS
        iops = None

        if volume_type == "io1":
            iops = SimpleTUI.input_dialog("IOPS limit",
                                          question="Specify the number of IOPS (I/O operations per second) the volume has to support," +
					            "[Min:100 | Max:64000 IOPS]",
                                          return_type=int)
            if iops is None or iops > 64000 or iops < 100:
                return
	

        # Zone selection
        zone_index = SimpleTUI.list_dialog("Zones available",
                                           self.print_all_availability_zones,
                                           question="Select a zone where the volume will be created")
        if zone_index is None:
            return
        zone = self.avail_zones[zone_index - 1]
        # Volume creation
        return self.ec2_client.create_volume(name=volume_name, size=volume_size, location=zone,
                                             ex_volume_type=volume_type, ex_iops=iops)

    def _platform_is_ip_assigned(self, floating_ip):
        """
        Check if a floating IP is assigned to an instance

        Args:
            floating_ip (<floating_ip>): The floating IP to check

        Returns:
            bool: True if the floating IP is assigned, False otherwise
        """
        if(floating_ip.instance_id is not None):
            return True
        return False

    def _platform_detach_floating_ip(self, floating_ip):
        """
        Detach a floating IP using the Amazon Web Services API

        Args:
            floating_ip (<floating_ip>): The floating IP to detach

        Returns:
            bool: True if the floating IP is successfully detached, False otherwise
        """
        result = self.ec2_client.ex_disassociate_address(floating_ip, domain="vpc")
        if(result):
            while True:
                # No direct way to refresh a Floating IP, so we look if
                # it is still attached to its previous instance
                node = self.ec2_client.list_nodes(ex_node_ids=[floating_ip.instance_id])[0]
                node_floating_ips = self.ec2_client.ex_describe_addresses_for_node(node)
                if floating_ip.ip not in node_floating_ips:
                    break
                time.sleep(3)
        return result

    def _platform_release_floating_ip(self, floating_ip):
        """
        Release a floating IP using the Amazon Web Services API

        Args:
            floating_ip (<floating_ip>): The floating IP to release

        Returns:
            bool: True if the floating IP is successfully released, False otherwise
        """
        return self.ec2_client.ex_release_address(floating_ip, domain="vpc")

    def _platform_associate_floating_ip(self, floating_ip, instance):
        """
        Associate a floating IP to an instance using the Amazon Web Services API

        Args:
            floating_ip (<floating_ip>): The floating IP to attach
            instance (<instance>): The instance where the floating IP is to be assigned

        Returns:
            bool: True if the floating IP is successfully associated, False otherwise
        """
        return self.ec2_client.ex_associate_address_with_node(instance, floating_ip)

    def _platform_reserve_floating_ip(self):
        """
        Reserve a floating IP using the Amazon Web Services API
        """
        return self.ec2_client.ex_allocate_address()

    def _platform_get_monitor(self, commands_queue, measurements_queue, metrics_file=None):
        """
        Create the AWS Monitor

        Args:
            commands_queue (Queue): message queue for communicating with the main
                                    thread and receiving commands regarding the metrics
                                    to observe
            measurements_queue (Queue): message queue for sending measurements to
                                        the platform RuleEngine
            metrics_file (str): path to the metrics file

        Returns:
            MetaMonitor: the platform-specific monitor
        """
        self.instances = self.list_instances()
        for instance in self.instances:
            commands_queue.put({"command": "add", "instance_id": instance.id})
        return AWSMonitor(conf=self.conf,
                          commands_queue=commands_queue,
                          measurements_queue=measurements_queue,
                          metrics_file=metrics_file)

    # =============================================================================================== #
    #                               Platform-specific Actions and Menus                               #
    # =============================================================================================== #

    def _get_security_groups_from_instance(self, instance):
        """
        Return the security groups list assigned to an instance

        Args:
            instance (Node): an AWS instance object

        Returns:
            str[]: List of names of security groups associated with the instance
        """
        security_groups = []
        if "groups" in instance.extra:
            for security_group in instance.extra["groups"]:
                security_groups.append(security_group["group_name"])
        return security_groups

    def _get_instance_type_from_instance(self, instance):
        """
        Return the instance type object

        Args:
            instance (Node): an AWS instance object

        Returns:
            NodeSize: an instance type object, None if it can't be determined
        """
        for instance_type in self.ec2_client.list_sizes():
            if instance_type.id == instance.extra["instance_type"]:
                return instance_type
        return None

    def _platform_extra_menu(self):
        """
        Print the extra Functions Menu (specific for each platform)
        """
        while(True):
            menu_header = self.platform_name + " Extra Commands"
            menu_subheader = ["Region: \033[1;94m" +
                              self._platform_get_region() + "\033[0m"]
            menu_items = ["Back to the Main Menu"]
            choice = SimpleTUI.print_menu(menu_header, menu_items, menu_subheader)
            # if choice == 1 and self._is_barebone():  # Blazar menu
            if choice == 1:
                break
            else:
                SimpleTUI.msg_dialog("Error", "Unimplemented functionality", SimpleTUI.DIALOG_ERROR)

    # =============================================================================================== #
    #                                         Override methods                                        #
    # =============================================================================================== #

    override_main_menu = []
    override_ips_menu = []
    override_volumes_menu = []

    def _platform_is_overridden(self, menu, choice):
        """
        Check if a menu voice is overridden by this platform

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            bool: the override status (True/False)
        """
        # if menu == "main" and choice in self.override_main_menu:
        #     if choice == 6 and self._is_barebone():  # Volumes on barebone are not supported
        #         return True
        #     elif choice in [8, 9] and not self._is_barebone():  # Monitor is not available on KVM
        #         return True
        return False

    def _platform_override_menu(self, menu, choice):
        """
        Override a menu voice

        Args:
            menu (str): a menu identifier
            choice (int): a menu entry index

        Returns:
            int: 0 for normal, 1 for going back to the main menu of EasyCloud,
                 2 for closing the whole application
        """
        # if menu == "main":
        #    if choice == 6:  # Volumes on barebone
        #        return 0
        #    SimpleTUI.error("Unavailable choice!")
        #    SimpleTUI.pause()
        #    SimpleTUI.clear_console()
        pass

    # =============================================================================================== #
    #                                  RuleEngine Actions Definition                                  #
    # =============================================================================================== #

    # All the actions are defined in the actions.py file in the module directory
    # Note: the platform name passed on the decorator must be equal to this class name

    @bind_action("AWS", "clone")
    def clone_instance(self, instance_id):
        """
        Clone instance
        """
        AWSAgentActions.clone_instance(self, instance_id)

    @bind_action("AWS", "alarm")
    def alarm(self, resource_id):
        """
        Trigger an alarm
        """
        AWSAgentActions.alarm(self, resource_id)
