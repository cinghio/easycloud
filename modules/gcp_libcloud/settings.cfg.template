[gcp]

# Google Cloud Platform Parameters
#
# API keys and default region definition. Keys can be generated using the Google Developer Console
# (https://cloud.google.com/console), under API and Services, Credentials, and then select "create 
# credentials followed by OAuth client ID. For application type select "Desktop App" and put a name
# (the name value is up to you). Click on "create" button and you will obtain "your client ID" and
# "your client secret" to paste into to gcp_access_key_id and gcp_secret_access_key respectively.
# The gpc_project is the id of the project that you can read by clicking on the project name at the top of the 
# web console, just next to "google cloud platfom" wording. The last field, gcp_datacenter can be "none".

gcp_access_key_id = 
gcp_secret_access_key =
gcp_project_id = 
gcp_datacenter = 

[alwaysfree]

# Free tier profile definition
#
# These settings define which instance types and images are available for
# Google Cloud Always Free Tier eligible users. Please note that you may 
# still be charged for the use of some gcp products unless your infrastructure 
# and service choices remain within the free usage tier.
# Please visit https://cloud.google.com/free/docs/gcp-free-tier for more info
#
# Always Free limits:
# * 30 GB HDD / month
# * 1 f1-micro instance / month
# * 5 GB snapshot / month
# * 1 GB Network traffic from NA to everywhere, except for CN/AU / month
# * 5 GB Cloud Storage

alwaysfree_only = true
alwaysfree_instance_types = f1-micro
# Not used at the moment
alwaysfree_zones = us-west1, us-central1, us-east1

[options]

# Time in seconds between measurements fetches
monitor_fetch_period = 60

# Granularity of the measurements in seconds
# Note: minimum value is 60 seconds
granularity = 60

# How many measurements are retrieved for a single metric each time
window_size = 5

# Minimum number of measurements (1-window_size) positive to a rule
# that must be positive in order to trigger an action
minimum_positive = 3

# Tells what measures sinks to enable
# This option takes a comma-separated list of labels, each of which is used to identify a specific "subsection" of this file, providing options specific to the asssociated measures sink.
# For instance:
#   measures_sinks = FOO, BAR
#   [measure_sink.FOO]
#   module = <fully qualified module name containing the class that implements FOO's measures sink>
#   class = <the name of the class that implements FOO's measures sink>
#   FOO_option1 = ...
#   FOO_option2 = ...
#   [measure_sink.BAR]
#   module = <fully qualified module name containing the class that implements BAR's measures sink>
#   class = <the name of the class that implements BAR's measures sink>
#   BAR_option1 = ...
#   BAR_option2 = ...
#   BAR_option3 = ...
# As shown in the above example, each subsection *must* contain at least two special options, namely:
# - module, whose value represents the fully qualified module name containing the class that implements a given measures sink, and
# - class, whose value represents the name of the class that implements a given measures sink.
measures_sinks = activemq, cassandra, file, kafka, rabbitmq, redis

# Settings related to file measures sinks

[measures_sink.activemq]

module = core.measures_sink.activemq
class = ActiveMQSink
# A comma-separated list of broker servers; for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:61613 
brokers = localhost:61613
# The optional login user
# Default: none
user =
# The optional password of the login user
# Default: none
password =
# The ActiveMQ destination (either a queue or a topic) where to publish measures
# collected from cloud platforms.
# Defaut: easycloud
destination = easycloud
# The ActiveMQ destination type; it can be either 'queue' or 'topic'.
# Defaut: topic
destination_type = topic

[measures_sink.cassandra]

module = core.measures_sink.cassandra
class = CassandraSink
# A comma-separated list of contact points to try connecting for cluster discovery; for instance:
# host1, host2:port2, host3
# Default: localhost
contact_points = localhost
# The server-side port to which Cassandra is listening
# Default: 9042
port = 9042
# The Cassandra keyspace where to collect measures.
# Default: easycloud
keyspace = easycloud
# Name of the Cassandra table (formerly known as column family), inside the
# above keyspace, where to collect measures.
# Note, the table name should not contain the keyspace qualifier
# Default: measures
table = measures

[measures_sink.file]

module = core.measures_sink.file
class = CsvFileSink
# The top-level path where to store CSV files.
# Default: <sys-temp-dir>/easycloud
basepath = /tmp/easycloud

[measures_sink.kafka]

module = core.measures_sink.kafka
class = KafkaSink
# A comma-separated list of broker servers for instance: broker1:port1, broker2:port2, broker3:port3
# Default: localhost:9092 
brokers = localhost:9092
# The Kafka topic where to publish measures collected from cloud platforms
# Defaut: easycloud
topic = easycloud

[measures_sink.rabbitmq]

module = core.measures_sink.rabbitmq
class = RabbitMQSink
# The URL to the RabbitMQ server (see `URLParameters` at https://pika.readthedocs.io/en/stable/modules/parameters.html#urlparameters)
# Default: amqp://localhost:5672
url = amqp://localhost:5672
# The RabbitMQ exchange to which sending messages (see https://www.rabbitmq.com/tutorials/amqp-concepts.html)
# Default: easycloud
exchange = easycloud

[measures_sink.redis]

module = core.measures_sink.redis
class = RedisSink
# The URL to the Redis server (see `redis.from_url()` at https://redis-py.readthedocs.io/en/latest/)
# Default: redis://localhost:6379/
url = redis://localhost:6379/
# The db parameter is the database number. You can manage multiple databases in Redis at once, and each is identified by an integer. The max number of databases is 16 by default
# Default: none
db = 0
